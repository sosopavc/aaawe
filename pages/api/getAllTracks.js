import { connectToDatabase } from '../../util/mongodb';

export default async (req, res) => {
  try {
    const { db } = await connectToDatabase();
    const tracks = await db.collection('data').find({}).toArray();
    res.status(200).json(tracks);
  } catch (error) {
    res.status(400).json(error);
  }
};
